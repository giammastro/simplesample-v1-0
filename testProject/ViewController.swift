//
//  ViewController.swift
//  testProject
//
//  Created by Giammarco Mastronardi on 07/11/2019.
//  Copyright © 2019 Giammarco Mastronardi. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController {
    var player1 = AVAudioPlayer()
    var player2 = AVAudioPlayer()
    var player3 = AVAudioPlayer()
    var player4 = AVAudioPlayer()
    var player5 = AVAudioPlayer()
    var player6 = AVAudioPlayer()
    var player7 = AVAudioPlayer()
    var player8 = AVAudioPlayer()
    var player9 = AVAudioPlayer()
//    var player10 = AVAudioPlayer()
//    var player11 = AVAudioPlayer()
//    var player12 = AVAudioPlayer()
    
    @IBOutlet weak var greetingsLabel: UILabel!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let sound1 = Bundle.main.path(forResource: "giorno-theme2", ofType: "wav")
        do {
            player1 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound1! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let sound2 = Bundle.main.path(forResource: "op-hihat4-trimd", ofType: "wav")
        // copy this syntax, it tells the compiler what to do when action is received
        do {
            player2 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound2! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound3 = Bundle.main.path(forResource: "cl-hihat4-trimd", ofType: "wav")
        do {
            player3 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound3! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound4 = Bundle.main.path(forResource: "giorno-bassnrhodes", ofType: "wav")
        do {
            player4 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound4! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound5 = Bundle.main.path(forResource: "op-hihat4-trimd", ofType: "wav")
        do {
            player5 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound5! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound6 = Bundle.main.path(forResource: "cl-hihat4-trimd", ofType: "wav")
        do {
            player6 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound6! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound7 = Bundle.main.path(forResource: "606-kick-trimd", ofType: "wav")
        do {
            player7 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound7! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound8 = Bundle.main.path(forResource: "snare14trimd", ofType: "wav")
        do {
            player8 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound8! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let sound9 = Bundle.main.path(forResource: "clap2-trimd", ofType: "wav")
        do {
            player9 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound9! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
//        super.viewDidLoad()
//        let sound10 = Bundle.main.path(forResource: "ASD_interrogative", ofType: "mp3")
//        do {
//            player10 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound10! ))
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
//            try AVAudioSession.sharedInstance().setActive(true)
//            }
//        catch{
//            print(error)
//        }
//
//        super.viewDidLoad()
//        let sound11 = Bundle.main.path(forResource: "ASD_interrogative", ofType: "mp3")
//        do {
//            player11 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound11! ))
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
//            try AVAudioSession.sharedInstance().setActive(true)
//            }
//        catch{
//            print(error)
//        }
//
//        super.viewDidLoad()
//        let sound12 = Bundle.main.path(forResource: "ASD_interrogative", ofType: "mp3")
//        do {
//            player12 = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound12! ))
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
//            try AVAudioSession.sharedInstance().setActive(true)
//            }
//        catch{
//            print(error)
//        }
        
    }
        
    
    
    
    @IBOutlet weak var button1Outlet: UIButton!
    @IBAction func button1(_ sender: Any) {
        if player1.isPlaying {
            player1.stop()
            button1Outlet.setImage(UIImage(named: "pad-pink"), for: UIControl.State.normal)
        } else {
            player1.currentTime = 0
            player1.play()
            player1.numberOfLoops = -1
            button1Outlet.setImage(UIImage(named: "pad-pink-lit"), for: UIControl.State.normal)
        }
    }
    
    @IBOutlet weak var button2Outlet: UIButton!
    @IBAction func button2(_ sender: Any) {
        player2.currentTime = 0
        player2.play()
    button2Outlet.setImage(UIImage(named:"pad-orange-lit"), for: UIControl.State.highlighted)
    }
    
    
    
    @IBOutlet weak var button3Outlet: UIButton!
    @IBAction func button3(_ sender: Any) {
        player3.currentTime = 0
        player3.play()
        player3.numberOfLoops = -1
    button3Outlet.setImage(UIImage(named:"pad-azure-lit"), for: UIControl.State.highlighted)
    }
    
    
    @IBAction func button3stop(_ sender: Any) {
        player3.stop()
    }
    
    @IBAction func button3stopOutside(_ sender: Any) {
        player3.stop()
    }
    
    @IBOutlet weak var button4Outlet: UIButton!
    @IBAction func button4(_ sender: Any) {
        if player4.isPlaying {
            player4.stop()
            button4Outlet.setImage(UIImage(named: "pad-pink"), for: UIControl.State.normal)
        } else {
            player4.currentTime = 0
            player4.play()
            player4.numberOfLoops = -1
            button4Outlet.setImage(UIImage(named: "pad-pink-lit"), for: UIControl.State.normal)
        }
    }
  
    @IBOutlet weak var button5Outlet: UIButton!
    @IBAction func button5(_ sender: Any) {
        player5.currentTime = 0
        player5.play()
    button5Outlet.setImage(UIImage(named:"pad-orange-lit"), for: UIControl.State.highlighted)
    }
    
    @IBOutlet weak var button6Outlet: UIButton!
    @IBAction func button6(_ sender: Any) {
        player6.currentTime = 0
        player6.play()
    button6Outlet.setImage(UIImage(named:"pad-orange-lit"), for: UIControl.State.highlighted)
    }
    
    
    @IBOutlet weak var button7Outlet: UIButton!
    @IBAction func button7(_ sender: Any) {
        player7.currentTime = 0
        player7.play()
    button7Outlet.setImage(UIImage(named:"pad-orange-lit"), for: UIControl.State.highlighted)
    }
    
    @IBOutlet weak var button8Outlet: UIButton!
    @IBAction func button8(_ sender: Any) {
        player8.currentTime = 0
        player8.play()
    button8Outlet.setImage(UIImage(named:"pad-orange-lit"), for: UIControl.State.highlighted)
    }
    
    @IBOutlet weak var button9Outlet: UIButton!
    @IBAction func button9(_ sender: Any) {
        player9.currentTime = 0
        player9.play()
    button9Outlet.setImage(UIImage(named:"pad-orange-lit"), for: UIControl.State.highlighted)
    }
    
    
//    @IBAction func button10(_ sender: Any) {
//        player10.currentTime = 0
//        player10.play()
//    }
//
//
//    @IBAction func button11(_ sender: Any) {
//        player11.currentTime = 0
//        player11.play()
//    }
//
//    @IBAction func button12(_ sender: Any) {
//        player12.currentTime = 0
//        player12.play()
//    }
    
    
    
    
    
    
    
    
    
    
    
    
//    @IBAction func buttonAction(_ sender: Any) {
//        audioPlayer.play()
//        greetingsLabel.text = "I WILL HAUNT YOUR DREAMS FOREVER"
//    }
//
//    @IBAction func royaltyButton(_ sender: Any) {
//        audioPlayer2.play()
//
//    }
}

